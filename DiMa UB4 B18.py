#!/usr/bin/python3
# coding=utf-8
"""
Diskrete Mathematik ÜB 4 Beispiel 18
Chinesischer Restsatz
"""

__author__ = "Lorenz Leitner"
__updated__ = "04-21-2015"


def getUserInput():
    num_array = []
    for i in range(0, 4):
        num_array.append(input())
    return num_array


def product(modules):
    p = 1
    for i in modules:
        p *= i
    return p


def main():
    global x
    modules_map = map(int, getUserInput())
    modules = list(modules_map)
    m = product(modules)

    for x in range(0, m):
        if (x % modules[0] == 1 % modules[0])\
                and (x % modules[1] == 2 % modules[1])\
                and (x % modules[2] == 0 % modules[2])\
                and (x % modules[3] == 3 % modules[3]):
            break

    print("x = %d + k * %d" % (x, m))

if __name__ == "__main__":
    main()

