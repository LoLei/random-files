#include <iostream>

int main()
{
  char myarray[10];

  std::cout << "Enter the characters." << std::endl;
  // Enter one character and press Enter
  for (int i = 0; i < 10; i++)
  {
    std::cin >> myarray[i];
  }

  std::cout << "Your array is: ";
  for (int i = 0; i < 10; i++)
  {
    std::cout << myarray[i];
  }
  std::cout << std::endl;
}
