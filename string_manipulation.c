// Asks your name and prints it in initials.

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MSG_OOM "Out of memory"

int main()
{
  char vname1[15];
  char vname2[15];
  char nname[15];
  char *fname;
  char period[] = ". ";

  puts("Enter your first name:");
  scanf("%s", &vname1);

  puts("Enter your middle name:");
  scanf("%s", &vname2);

  puts("Enter your last name:");
  scanf("%s", &nname);

  printf("Your name is: %s %s %s\n", vname1, vname2, nname);

  fname = (char *)malloc((strlen(nname)+7) * sizeof(char));
  if (fname  == NULL)
  {
    perror(MSG_OOM);
    exit(1);
  }

  fname[0] = '\0';

  strncat(fname, vname1, 1);
  strcat(fname, period);
  strncat(fname, vname2, 1);
  strcat(fname, period);
  strcat(fname, nname);
  printf("Or: %s\n", fname);

  free(fname);

  return 0;
}