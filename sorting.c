// Prompts a user to enter numbers, stops if he enters 0
// TODO: Sorting algorithm

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

#define MSG_OOM "Out of memory"
#define MSG_FILE "Could not read or write file"
#define MAXNUMBERS 5

int getNumbers(int array[])
{
  int i;
  int n = 0;
  printf("Enter max. %d numbers, enter 0 to end:\n", MAXNUMBERS);

  for (i = 0; i < MAXNUMBERS; i++)
  {
    scanf("%d", &array[i]);
    fflush(stdin);
    if (array[i] == 0)
    {
      break;
    }
    n++;
  }
  return n;
}

int main()
{
  int array[MAXNUMBERS];
  int amount_numbers;
  amount_numbers = getNumbers(array);

  printf("Numbers entered: %d\n", amount_numbers);
  printf("First three: %d %d %d\n", array[0], array[1], array[2]);

  return 0;
}