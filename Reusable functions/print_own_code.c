#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

#define MSG_OOM "Out of memory"
#define MSG_FILE "Could not read or write file"

int main()
{
  
  char *source_code;
  source_code = (char *)malloc(1000 * sizeof(char));
  if (source_code == NULL)
  {
    perror(MSG_OOM);
    return (-1);
  }

  FILE *fp;
  fp = fopen("print_own_code.c", "r");
  if (fp == NULL)
  {
    perror(MSG_FILE);
    return(-1);
  }
  
  int ch = 0;
  int i = 0;
  int k = 0;

  while ((ch = fgetc(fp)) != EOF)
  {
    if (k >= 1000)
    {
      source_code = (char *)realloc(source_code, (i+1000) * sizeof(char));
      k = 0;
    }
    if (source_code == NULL)
    {
      perror(MSG_OOM);
      return (-1);
    }

    source_code[i] = ch;
    i++;
    k++;
  }
  source_code[i] = '\0';

  fprintf(stdout, source_code);
  free(source_code);

  return 0;
}