// Lorenz Leitner 02/26/2015
// Open file and copy letters to upper-case in new file
// TODO:  
//        Let user choose file path
// DONE:
//        02/26/2015 Made array into pointer array with malloc and realloc
//        02/28/2015 Added one string for each case of the prompt to enter a name
//        03/02/2015 Added free()
//        03/02/2015 Added errno to perror
//        03/03/2015 Checked what file names user can enter
//        03/04/2015 User can enter Win7 standard file name length

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MSG_OOM "Out of memory"
#define MSG_FILE "Could not read or write file"
#define TYPE_OLD 0
#define TYPE_NEW 1

void getName(char *name, int type)
{
  // Function to prompt user to enter either name of old or new file
  char old_string[] = "old";
  char new_string[] = "new";
  char *string;
  string = (char *)malloc(strlen(old_string) + 1);
  if (string == NULL)
  {
    perror(MSG_OOM);
    exit(1);
  }

  if (type == 0) // Type is given in the function call
  {
    strcpy(string, old_string);
  }
  else
  {
    strcpy(string, new_string);
  }

  printf("Enter the name of the %s file: (Max. 30 characters)\n", string);
  printf("Example: file_%s.txt\n", string);
  free(string);
  printf(">>");
  scanf("%s", name);
}

int main()
{
  FILE *old_file; // File pointer for old file
  int buffer; // Buffer for each character

  char *array; // Array to store each character
  array = (char *)malloc(500 * sizeof(char)); // Allocating space for the array
  if (array == NULL)
  {
    perror(MSG_OOM);
    exit(1);
  }

  char old_name[231]; // Old file name
  char new_name[231]; // New file name
  int n = 0; // counts characters later
  int i = 0; // Index for the array
  int k = 0; // Used for reallocating

  puts("This programm will copy the content of the old file into a new file \
                     and convert all letters to upper-case.");
  puts("You can use *.txt files or other files.");
  puts("");

  getName(old_name, TYPE_OLD); // Calls the function prompting the user to enter the name of the old file
  getName(new_name, TYPE_NEW); // Calls the function prompting the user to enter the name of the new file

  if ((old_file = fopen(old_name, "r")) == NULL) // Opens old file
  {
    perror(MSG_FILE);
    exit(1);
  }

  while ((buffer = getc(old_file)) != EOF) // Buffer is each character in each iteration of the loop until it is at the "End of file"
  {
    if (buffer >= 97 && buffer <= 122) // This makes sure only "letters" are converted, not other symbols
    {
      buffer = buffer - 32; // Each lower-case letter -32 is the ASCII value of the corresponding upper-case letter
    }
    if (k >= 500) // If there are more than 500 characters, reallocate space
    {
      array = (char *)realloc(array, (n + 500) * sizeof(char));
      k = 0;
    }
    putchar(buffer); // Also print each character now
    array[i] = buffer; // Store each caracter in the array for later
    n++;
    i++;
    k++;
  }
  fclose(old_file);

  puts("");
  printf("Character count: %d\n", n);

  FILE *new_file; // Open new file with new file pointer
  if ((new_file = fopen(new_name, "wb")) == NULL)
  {
    perror(MSG_FILE);
    exit(1);
  }

  if ((fwrite(array, sizeof(char), n, new_file)) != n) // Each element of the array is written into the new file
  {
    perror(MSG_FILE);
    exit(1);
  }
  free(array);
  fclose(new_file);
  return 0;
}