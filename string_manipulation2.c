// Counts how many times a string is in another string

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MSG_OOM "Out of memory"

int main()
{
  char *string1 = "gabegabegabegabe";
  char *string2 = "gabe";
  int n = 0;

  while ((string1 = strstr(string1, string2)) != NULL)
  {
    string1++;
    n++;
  }

  printf("%d", n);

  return 0;
}